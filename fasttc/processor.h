#pragma once
								 
#include "DXUT.h"
#include "DirectXTex.h"
#include "tchar.h"
#include "ispc_texcomp.h"
#include "StopWatch.h"
#include "utils.h"
#include <memory>
#include <strsafe.h>

using namespace DirectX;

//typedef unsigned char BYTE;
typedef void (CompressionFunc)(const rgba_surface* input, BYTE* output);

extern CompressionFunc* gCompressionFuc;
extern bool multithreaded;

int GetBytesPerBlock(CompressionFunc* fn);
DXGI_FORMAT GetFormatFromCompressionFunc(CompressionFunc* fn);
string inputFileName;
string outputFileName;

void CompressImageBC1(const rgba_surface* input, BYTE* output);
void CompressImageBC3(const rgba_surface* input, BYTE* output);
void CompressImageBC7_ultrafast(const rgba_surface* input, BYTE* output);
void CompressImageBC7_veryfast(const rgba_surface* input, BYTE* output);
void CompressImageBC7_fast(const rgba_surface* input, BYTE* output);
void CompressImageBC7_basic(const rgba_surface* input, BYTE* output);
void CompressImageBC7_slow(const rgba_surface* input, BYTE* output);
void CompressImageBC7_alpha_ultrafast(const rgba_surface* input, BYTE* output);
void CompressImageBC7_alpha_veryfast(const rgba_surface* input, BYTE* output);
void CompressImageBC7_alpha_fast(const rgba_surface* input, BYTE* output);
void CompressImageBC7_alpha_basic(const rgba_surface* input, BYTE* output);
void CompressImageBC7_alpha_slow(const rgba_surface* input, BYTE* output);

Image img;
BYTE out;

void CompressTexture(rgba_surface input, BYTE *out);
void RemapTexture(rgba_surface input, BYTE *img);
void SaveTexture(HRESULT hr, rgba_surface input, BYTE *img);
rgba_surface PadTextue(rgba_surface input);

void InitWin32Threads();
void DestroyThreads();
VOID CompressImageMT(const rgba_surface* input, BYTE* output);
DWORD WINAPI CompressImageMT_Thread(LPVOID lpParam);

HRESULT initWIC();

