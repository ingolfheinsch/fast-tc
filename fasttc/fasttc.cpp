// fasttc.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "processor.h"
#include <memory>

using namespace std;

DXGI_FORMAT cformat = DXGI_FORMAT_BC7_UNORM;
CompressionFunc* gCompressionFunc = CompressImageBC7_ultrafast;
bool multithreaded = true;

DWORD gNumProcessors = 1;
double gCompRate = 0.0;
const int kNumCompressions = 1;
// Win32 thread API
const int kMaxWinThreads = 64;

enum EThreadState {
	eThreadState_WaitForData,
	eThreadState_DataLoaded,
	eThreadState_Running,
	eThreadState_Done
};

struct WinThreadData {
	EThreadState state;
	int threadIdx;
	CompressionFunc* cmpFunc;
	rgba_surface input;
	BYTE *output;

	// Defaults..
	WinThreadData() :
		state(eThreadState_Done),
		threadIdx(-1),
		input(),
		output(NULL),
		cmpFunc(NULL)
	{ }

} gWinThreadData[kMaxWinThreads];

HANDLE gWinThreadWorkEvent[kMaxWinThreads];
HANDLE gWinThreadStartEvent = NULL;
HANDLE gWinThreadDoneEvent = NULL;
int gNumWinThreads = 0;
DWORD dwThreadIdArray[kMaxWinThreads];
HANDLE hThreadArray[kMaxWinThreads];


void CompressImageST(const rgba_surface* input, BYTE* output)
{
	if (gCompressionFunc == NULL) {
		OutputDebugString(L"DXTC::CompressImageDXTNoThread -- Compression Scheme not implemented!\n");
		return;
	}
	cout << "compress" << endl;

	// Do the compression.
	(*gCompressionFunc)(input, output);
}

DXGI_FORMAT GetFormatFromCompressionFunc(CompressionFunc* fn)
{
	if (fn == CompressImageBC1) return DXGI_FORMAT_BC1_UNORM_SRGB;
	if (fn == CompressImageBC3) return DXGI_FORMAT_BC3_UNORM_SRGB;

	return DXGI_FORMAT_BC7_UNORM_SRGB;
}

Image createImage(BYTE *input, int width, int height, DXGI_FORMAT format)
{

	//max(1, ((width + 3) / 4)) * block - size
		//The block - size is 8 bytes for DXT1, BC1, and BC4 formats, and 16 bytes for other block - compressed formats.
	int rpitch = max(1, ((width + 3) / 4)) * GetBytesPerBlock(gCompressionFunc);
	int spitch = (width * height) * 4;

	cout << "rpitch: " << rpitch << endl;
	cout << "spitch: " << spitch << endl;
	cout << "format: " << format << endl;

	Image img;
	img.width = width;
	img.height = height;
	img.format = format;
	img.rowPitch = rpitch; //4 * (32 * 4 + 3) / 4;/*<number of bytes in a scanline of the source data>*/
	img.slicePitch = spitch;/*<number of bytes in the entire 2D image>*/
	img.pixels = input; /*<pointer to pixel data>*/
	
	return img;
}

int GetBytesPerBlock(CompressionFunc* fn)
{
	DXGI_FORMAT format = GetFormatFromCompressionFunc(fn);

	switch (format)
	{
	default:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
		return 8;

	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC7_UNORM:
	case DXGI_FORMAT_BC7_UNORM_SRGB:
		return 16;
	}
}

HRESULT initWIC()
{
	// Initialize COM (needed for WIC)
	HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
	if (FAILED(hr))
	{
		cerr << "Failed to initialize COM " << hr << endl;
		exit(1);
	}
	else
	{
		cout << "COM Pointer initalized!" << hr << endl;
		return hr;
	}
}

void initialize()
{
	// Make sure that the event array is set to null...
	memset(gWinThreadWorkEvent, 0, sizeof(gWinThreadWorkEvent));

	// Figure out how many cores there are on this machine
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	gNumProcessors = sysinfo.dwNumberOfProcessors;

	// Make sure all of our threads are empty.
	for (int i = 0; i < kMaxWinThreads; i++) {
		hThreadArray[i] = NULL;
	}
}

#define CHECK_WIN_THREAD_FUNC(x) \
	do { \
		if(NULL == (x)) { \
			wchar_t wstr[256]; \
			swprintf_s(wstr, L"Error detected from call %s at line %d of main.cpp", _T(#x), __LINE__); \
			ReportWinThreadError(wstr); \
		} \
	} \
	while(0)

void ReportWinThreadError(const wchar_t *str) {

	// Retrieve the system error message for the last-error code.
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message.

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)str) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		str, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	// Free error-handling buffer allocations.

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

void InitWin32Threads() {

//	cout << "numthreads:" << gNumWinThreads << endl;
	// Already initialized?
	if (gNumWinThreads > 0) {
		return;
	}

	SetLastError(0);

	gNumWinThreads = gNumProcessors;
	

	if (gNumWinThreads >= MAXIMUM_WAIT_OBJECTS)
		gNumWinThreads = MAXIMUM_WAIT_OBJECTS;

	assert(gNumWinThreads <= kMaxWinThreads);

	// Create the synchronization events.
	for (int i = 0; i < gNumWinThreads; i++) {
		CHECK_WIN_THREAD_FUNC(gWinThreadWorkEvent[i] = CreateEvent(NULL, FALSE, FALSE, NULL));
	}

	CHECK_WIN_THREAD_FUNC(gWinThreadStartEvent = CreateEvent(NULL, TRUE, FALSE, NULL));
	CHECK_WIN_THREAD_FUNC(gWinThreadDoneEvent = CreateEvent(NULL, TRUE, FALSE, NULL));

	// Create threads
	for (int threadIdx = 0; threadIdx < gNumWinThreads; threadIdx++) {
		gWinThreadData[threadIdx].state = eThreadState_WaitForData;
		CHECK_WIN_THREAD_FUNC(hThreadArray[threadIdx] = CreateThread(NULL, 0, CompressImageMT_Thread, &gWinThreadData[threadIdx], 0, &dwThreadIdArray[threadIdx]));
	}
}

void DestroyThreads()
{
	if (multithreaded)
	{
		// Release all windows threads that may be active...
		for (int i = 0; i < gNumWinThreads; i++) {
			gWinThreadData[i].state = eThreadState_Done;
		}

		// Send the event for the threads to start.
		CHECK_WIN_THREAD_FUNC(ResetEvent(gWinThreadDoneEvent));
		CHECK_WIN_THREAD_FUNC(SetEvent(gWinThreadStartEvent));

		// Wait for all the threads to finish....
		DWORD dwWaitRet = WaitForMultipleObjects(gNumWinThreads, hThreadArray, TRUE, INFINITE);
		if (WAIT_FAILED == dwWaitRet)
			ReportWinThreadError(L"DestroyThreads() -- WaitForMultipleObjects");

		// !HACK! This doesn't actually do anything. There is either a bug in the 
		// Intel compiler or the windows run-time that causes the threads to not
		// be cleaned up properly if the following two lines of code are not present.
		// Since we're passing INFINITE to WaitForMultipleObjects, that function will
		// never time out and per-microsoft spec, should never give this return value...
		// Even with these lines, the bug does not consistently disappear unless you
		// clean and rebuild. Heigenbug?
		//
		// If we compile with MSVC, then the following two lines are not necessary.
		else if (WAIT_TIMEOUT == dwWaitRet)
			OutputDebugString(L"DestroyThreads() -- WaitForMultipleObjects -- TIMEOUT");

		// Reset the start event
		CHECK_WIN_THREAD_FUNC(ResetEvent(gWinThreadStartEvent));
		CHECK_WIN_THREAD_FUNC(SetEvent(gWinThreadDoneEvent));

		// Close all thread handles.
		for (int i = 0; i < gNumWinThreads; i++) {
			CHECK_WIN_THREAD_FUNC(CloseHandle(hThreadArray[i]));
		}

		for (int i = 0; i < kMaxWinThreads; i++) {
			hThreadArray[i] = NULL;
		}

		// Close all event handles...
		CHECK_WIN_THREAD_FUNC(CloseHandle(gWinThreadDoneEvent));
		gWinThreadDoneEvent = NULL;

		CHECK_WIN_THREAD_FUNC(CloseHandle(gWinThreadStartEvent));
		gWinThreadStartEvent = NULL;

		for (int i = 0; i < gNumWinThreads; i++) {
			CHECK_WIN_THREAD_FUNC(CloseHandle(gWinThreadWorkEvent[i]));
		}

		for (int i = 0; i < kMaxWinThreads; i++) {
			gWinThreadWorkEvent[i] = NULL;
		}

		gNumWinThreads = 0;
	}
}

VOID CompressImageMT(const rgba_surface* input, BYTE* output)
{
	const int numThreads = gNumWinThreads;
	const int bytesPerBlock = GetBytesPerBlock(gCompressionFunc);

	cout << "threadcount: " << numThreads << endl;

	// We want to split the data evenly among all threads.
	const int linesPerThread = (input->height + numThreads - 1) / numThreads;

	if (gCompressionFunc == NULL) {
		OutputDebugString(L"DXTC::CompressImageDXTNoThread -- Compression Scheme not implemented!\n");
		return;
	}

	// Load the threads.
	for (int threadIdx = 0; threadIdx < numThreads; threadIdx++)
	{
		int y_start = (linesPerThread*threadIdx) / 4 * 4;
		int y_end = (linesPerThread*(threadIdx + 1)) / 4 * 4;
		if (y_end > input->height) y_end = input->height;

		WinThreadData *data = &gWinThreadData[threadIdx];
		data->input = *input;
		data->input.ptr = input->ptr + y_start * input->stride;
		data->input.height = y_end - y_start;
		data->output = output + (y_start / 4) * (input->width / 4) * bytesPerBlock;
		data->cmpFunc = gCompressionFunc;
		data->state = eThreadState_DataLoaded;
		data->threadIdx = threadIdx;
	}

	// Send the event for the threads to start.
	CHECK_WIN_THREAD_FUNC(ResetEvent(gWinThreadDoneEvent));
	CHECK_WIN_THREAD_FUNC(SetEvent(gWinThreadStartEvent));

	// Wait for all the threads to finish
	if (WAIT_FAILED == WaitForMultipleObjects(numThreads, gWinThreadWorkEvent, TRUE, INFINITE))
		ReportWinThreadError(TEXT("CompressImageDXTWIN -- WaitForMultipleObjects"));

	// Reset the start event
	CHECK_WIN_THREAD_FUNC(ResetEvent(gWinThreadStartEvent));
	CHECK_WIN_THREAD_FUNC(SetEvent(gWinThreadDoneEvent));
}

DWORD WINAPI CompressImageMT_Thread(LPVOID lpParam) {
	WinThreadData *data = (WinThreadData *)lpParam;

	while (data->state != eThreadState_Done) {

		if (WAIT_FAILED == WaitForSingleObject(gWinThreadStartEvent, INFINITE))
			ReportWinThreadError(TEXT("CompressImageDXTWinThread -- WaitForSingleObject"));

		if (data->state == eThreadState_Done)
			break;

		data->state = eThreadState_Running;
		(*(data->cmpFunc))(&data->input, data->output);

		data->state = eThreadState_WaitForData;

		HANDLE workEvent = gWinThreadWorkEvent[data->threadIdx];
		if (WAIT_FAILED == SignalObjectAndWait(workEvent, gWinThreadDoneEvent, INFINITE, FALSE))
			ReportWinThreadError(TEXT("CompressImageDXTWinThread -- SignalObjectAndWait"));
	}

	return 0;
}


int main(int argc, char *argv[])
{
	initialize();
	if (multithreaded)
	{
		InitWin32Threads();
	}

	cout << "Fast Texture Compressor" << std::endl;
	cout << "" << std::endl;
	if (argv[1] == NULL)
	{
		cout << "you must specify filename. try -help." << endl;
		return 1;
	}
	else if (string(argv[1]) == "-help" )
	{
		cout << "Syntax: fasttc.exe [filename] [format {bc1 | bc3 | bc7-rgb-uf | bc7-rgb-vf | bc7-rgb-fa | bc7-rgb-ba | bc7-rgb-sl | bc7-rgba-uf | bc7-rgba-vf | bc7-rgba-fa | bc7-rgba-ba | bc7-rgba-sl}" << endl << endl;
		cout << "Web: http://bitbucket.org/ingolfheinsch/fasttc/" << endl;
		return 1;
	}
	else
	{
		inputFileName = string(argv[1]);
		outputFileName = RemoveFileExtension(inputFileName) + ".dds";
	}

	if (argv[2] == NULL)
	{
		cout << "you must specify compression format. try -help." << endl;
		return 1;
	}
	else
	{
		string format = string(argv[2]);
		
		if (string(argv[2]) == "bc7-rgba-uf")
		{
			gCompressionFunc = CompressImageBC7_alpha_ultrafast;
		}
		else if (string(argv[2]) == "bc7-rgba-vf")
		{
			gCompressionFunc = CompressImageBC7_alpha_veryfast;
		}
		else if (string(argv[2]) == "bc7-rgba-fa")
		{
			gCompressionFunc = CompressImageBC7_alpha_fast;
		}
		else if (string(argv[2]) == "bc7-rgba-ba")
		{
			gCompressionFunc = CompressImageBC7_alpha_basic;
		}
		else if (string(argv[2]) == "bc7-rgba-sl")
		{
			gCompressionFunc = CompressImageBC7_alpha_slow;
		}
		else if (string(argv[2]) == "bc7-rgb-uf")
		{
			gCompressionFunc = CompressImageBC7_ultrafast;
		}
		else if (string(argv[2]) == "bc7-rgb-vf")
		{
			gCompressionFunc = CompressImageBC7_veryfast;
		}
		else if (string(argv[2]) == "bc7-rgb-fa")
		{
			gCompressionFunc = CompressImageBC7_fast;
		}
		else if (string(argv[2]) == "bc7-rgb-ba")
		{
			gCompressionFunc = CompressImageBC7_basic;
		}
		else if (string(argv[2]) == "bc7-rgb-sl")
		{
			gCompressionFunc = CompressImageBC7_slow;
		}
		else if (string(argv[2]) == "bc3")
		{
			gCompressionFunc = CompressImageBC3;
		}
		else if (string(argv[2]) == "bc1")
		{
			gCompressionFunc = CompressImageBC1;
		}
		else 
		{
			cout << "no valid format " << endl;
			return 1;
		}
	}
	cout << "detected cores: " << gNumProcessors << std::endl;

	HRESULT hr = initWIC();

	TexMetadata info;
	std::unique_ptr<ScratchImage> image(new (nothrow) ScratchImage);

	hr = LoadFromWICFile(s2ws(inputFileName).c_str(), WIC_FLAGS_FORCE_RGB, &info, *image);
	if (FAILED(hr))
	{
		cerr << "Failed to read image" << " Code: " << hr << endl;
		return 1;
	}

	rgba_surface input;
	input.ptr = image->GetPixels();
	input.width = info.width;
	input.height = info.height;
	input.stride = input.width * 4;

	cout << "format: " << info.format << endl;
	cout << "width: " << info.width << "px" << endl;
	cout << "height: " << info.height << "px" << endl;
	cout << "dimension: " << info.dimension << endl;
	cout << "stride: " << input.stride << endl;

	unique_ptr<Image> img(new (std::nothrow) Image);

	if (!img)
	{
		cerr << " ERROR: Memory allocation failed" << endl;
		exit(1);
	}

	img->format = GetFormatFromCompressionFunc(gCompressionFunc);
	img->height = info.height;
	img->width = info.width;

	// pad texture if dimensions are not divisible by 4.
	if ((input.width % 4 != 0) || (input.height % 4 != 0))
	{
		cout << "texture not dividable by 4. start padding." << endl;
		
		input = PadTextue(input);
		//input = padTex;
		cout << "pad-width: " << input.width << "px" << endl;
		cout << "pad-height: " << input.height << "px" << endl;
	}
	BYTE* output;
	const int spitch = (input.width * input.height) * 4;
	output = new BYTE[spitch];

	StopWatch stopwatch;

	stopwatch.Start();
	CompressTexture(input, output);
	RemapTexture(input, output);
	stopwatch.Stop();
	cout << "compession time: " << stopwatch.TimeInMilliseconds() << "ms " << endl;
	
	// Compute the compression rate.
	INT numPixels = input.width * input.height * kNumCompressions;
	gCompRate = (double)numPixels / stopwatch.TimeInSeconds() / 1000000.0;
	
	cout << "compession rate: " << gCompRate << endl;
	SaveTexture(hr, input, output);

	return 0;
}

rgba_surface PadTextue(rgba_surface input)
{

	rgba_surface padTex;
	// Compute the size of the padded texture.
	UINT padWidth = input.width / 4 * 4 + 4;
	UINT padHeight = input.height / 4 * 4 + 4;
	cout << "pad-width: " << padWidth << "px" << endl;
	cout << "pad-height: " << padHeight << "px" << endl;
	// Create a buffer for the padded texels.
	BYTE* padTexels = new BYTE[padWidth * padHeight * 4];

	// Copy the beginning of each row.
	BYTE* texels = (BYTE*)input.ptr;

	for (UINT row = 0; row < input.height; row++)
	{
		UINT rowStart = row * input.stride;
		UINT padRowStart = row * padWidth * 4;
		memcpy(padTexels + padRowStart, texels + rowStart, input.width * 4);

		// Pad the end of each row.
		if (padWidth > input.width)
		{
			BYTE* padVal = texels + rowStart + (input.width - 1) * 4;
			for (UINT padCol = input.width; padCol < padWidth; padCol++)
			{
				UINT padColStart = padCol * 4;
				memcpy(padTexels + padRowStart + padColStart, padVal, 4);
			}
		}
	}

	// Pad the end of each column.
	if (padHeight > input.height)
	{
		UINT lastRow = (input.height - 1);
		UINT lastRowStart = lastRow * padWidth * 4;
		BYTE* padVal = padTexels + lastRowStart;
		for (UINT padRow = input.height; padRow < padHeight; padRow++)
		{
			UINT padRowStart = padRow * padWidth * 4;
			memcpy(padTexels + padRowStart, padVal, padWidth * 4);
		}
	}

	//create padded surface
	padTex.ptr = padTexels;
	padTex.width = padWidth;
	padTex.height = padHeight;
	padTex.stride = padWidth * sizeof(BYTE) * 4;
	
	// Delete the padded texel buffer.
	//delete[] padTexels;
	return padTex;
}

void CompressTexture(rgba_surface input, BYTE *output)
{
	
	for (int cmpNum = 0; cmpNum < kNumCompressions; cmpNum++)
	{
		// Compress the uncompressed texels.
	//	CompressImageST(&input, output);
		// If we aren't multi-cored, then just run everything serially.
		if (gNumProcessors <= 1 || !multithreaded)
		{
			cout << "start single threaded compression" << endl;
			CompressImageST(&input, output);
		}
		else
		{
			cout << "start muti threaded compression" << endl;
			CompressImageMT(&input, output);
		}

	}
}

void RemapTexture (rgba_surface input, BYTE *img)
{
	int output_stride = (input.width / 4) * GetBytesPerBlock(gCompressionFunc);
	cout << "output_stride: " << output_stride << endl;

	for (int y = input.height / 4 - 1; y >= 0; y--)
	{
		memmove(img + y* (input.width * 4), img + y*output_stride, output_stride);
	}
}

void SaveTexture(HRESULT hr, rgba_surface input, BYTE *img)
{
	hr = SaveToDDSFile(createImage(img, input.width, input.height, cformat), DDS_FLAGS_NONE, s2ws(outputFileName).c_str());
	if (FAILED(hr))
		cout << "error saving file";
	else
		cout << outputFileName.c_str() << " saved." << endl;
}

void CompressImageBC1(const rgba_surface* input, BYTE* output)
{
	CompressBlocksBC1(input, output);
}

void CompressImageBC3(const rgba_surface* input, BYTE* output)
{
	CompressBlocksBC3(input, output);
}

#define DECLARE_CompressImageBC7_profile(profile)								\
void CompressImageBC7_ ## profile(const rgba_surface* input, BYTE* output)		\
{																				\
	bc7_enc_settings settings;													\
	GetProfile_ ## profile(&settings);											\
	CompressBlocksBC7(input, output, &settings);								\
}

DECLARE_CompressImageBC7_profile(ultrafast);
DECLARE_CompressImageBC7_profile(veryfast);
DECLARE_CompressImageBC7_profile(fast);
DECLARE_CompressImageBC7_profile(basic);
DECLARE_CompressImageBC7_profile(slow);
DECLARE_CompressImageBC7_profile(alpha_ultrafast);
DECLARE_CompressImageBC7_profile(alpha_veryfast);
DECLARE_CompressImageBC7_profile(alpha_fast);
DECLARE_CompressImageBC7_profile(alpha_basic);
DECLARE_CompressImageBC7_profile(alpha_slow);