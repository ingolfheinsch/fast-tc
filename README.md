# Fast Texture Compressor #

State of the art texture compression for BC6H, BC7, ETC1 and ASTC. Based on Intel ISPC Compiler [FastTC](https://github.com/GameTechDev/ISPCTextureCompressor). This Version uses WIC Codec.

### syntax ###
`fasttc.exe [filename] [format {bc1 | bc3 | bc7-rgb-uf | bc7-rgb-vf | bc7-rgb-fa | bc7-rgb-ba | bc7-rgb-sl | bc7-rgba-uf | bc7-rgba-vf | bc7-rgba-fa | bc7-rgba-ba | bc7-rgba-sl}`

format switch offers various compression-time vs quality trade-offs.

### TODO ###

* add BC6H support
* add error computation
* visualize computation error
* add ASTC Codec

### Compression Time Samples ###

####4096x4096@24bpp, uncompressed TIF to BC7_UNORM@RGB, i7/1C@2.3GHz, Singlethreaded####

* SLOW: 122000ms
* BASIC: 35000ms
* FAST: 14000ms
* VERYFAST: 8500ms
* ULTRAFAST: 650ms

####4096x4096@24bpp, uncompressed TIF to BC7_UNORM@RGB, i7/4C@2.3GHz, Multithreaded ####

* SLOW: 30100ms
* BASIC: 7800ms
* FAST: 3100ms
* VERYFAST: 1850ms
* ULTRAFAST: 190ms